class Potepan::CategoriesController < ApplicationController
  def show
    @taxonomies = Spree::Taxonomy.includes(:root)
    @taxon = Spree::Taxon.includes(products:
      { master: [:default_price, :images] }).find(params[:taxon_id])
  end
end
