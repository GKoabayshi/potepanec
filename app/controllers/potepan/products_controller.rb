class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.includes(:variants).find(params[:id])
  end
end
