FactoryBot.define do
  factory :product, class: 'Spree::Product' do
    sequence(:name) { |n| "test_product_#{n}" }
    price { 100 }
    shipping_category_id { 1 }
    sequence(:slug) { |n| "test_slug_#{n}" }

    trait :with_product_properties do
      after(:build) do |product|
        product.product_properties << build(:product_property, value: "test_value")
      end
    end
  end
end
