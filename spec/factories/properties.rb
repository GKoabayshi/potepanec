FactoryBot.define do
  factory :property, class: 'Spree::Property' do
    name { "Type" }
    presentation { "Type" }
  end
end
