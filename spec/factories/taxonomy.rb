FactoryBot.define do
  factory :taxonomy, class: 'Spree::Taxonomy' do
    sequence(:name) { |n| "test_taxonomy_#{n}" }

    trait :with_taxons_and_products do
      after(:build) do |taxonomy|
        taxonomy.root = create(:taxon, parent_id: nil)
        taxonomy.taxons << create_list(:taxon, 3, :with_products, parent_id: taxonomy.root.id)
      end
    end
  end
end
