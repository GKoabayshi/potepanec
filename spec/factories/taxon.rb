FactoryBot.define do
  factory :taxon, class: 'Spree::Taxon' do
    sequence(:name) { |n| "test_taxon_#{n}" }

    trait :with_products do
      after(:build) do |taxon|
        taxon.products << create(:product)
      end
    end
  end
end
