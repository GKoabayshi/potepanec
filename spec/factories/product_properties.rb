FactoryBot.define do
  factory :product_property, class: 'Spree::ProductProperty' do
    value { "Tote" }
    association :property
  end
end
