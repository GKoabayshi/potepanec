require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "GET #show" do
    let(:product) { create(:product) }

    before do
      get :show, params: { id: product.id }
    end

    it "responds successfully" do
      expect(response).to be_successful
    end
    it "renders show template correctly" do
      expect(response).to render_template(:show)
    end
    it "sets product correctly" do
      expect(assigns(:product)).to eq product
    end
  end
end
