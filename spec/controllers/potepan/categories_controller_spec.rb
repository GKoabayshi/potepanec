require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "GET #show" do
    let(:taxon) { create(:taxon) }
    let(:taxonomies) { create_list(:taxonomy, 3) }

    before do
      get :show, params: { taxon_id: taxon.id }
    end

    it "responds successfully" do
      expect(response).to be_successful
    end

    it "renders show template successfully" do
      expect(response).to render_template(:show)
    end

    it "sets taxon correctly" do
      expect(assigns(:taxon)).to eq taxon
    end

    it "sets taxnomies correctly" do
      expect(assigns(:taxonomies)).to eq taxonomies
    end
  end
end
