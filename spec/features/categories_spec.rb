require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let!(:taxonomies) { create_list(:taxonomy, 2, :with_taxons_and_products) }
  let(:taxon) { Spree::Taxon.leaves.sample }
  let(:taxons) { Spree::Taxon.leaves }
  let(:wrong_taxon) { Spree::Taxon.where.not(id: taxon.id).leaves.sample }

  background do
    visit potepan_category_path(taxon.id)
  end

  scenario "all correct taxonomies, taxons, products are present" do
    expect(page).to have_title "#{taxon.name} - BIGBAG Store"

    within '.sideBar' do
      taxonomies.each do |taxonomy|
        expect(page).to have_content taxonomy.name
      end

      taxons.each do |taxon|
        expect(page).to have_content taxon.name
      end
    end

    taxon.products.each do |product|
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
    end
  end

  scenario "click product name then go to correct page" do
    taxon.products.each do |product|
      visit potepan_category_path(taxon.id)
      click_link product.name
      expect(page).to have_title "#{product.name} - BIGBAG Store"
    end
  end

  scenario "incorrect products are not present" do
    wrong_taxon.products.each do |product|
      expect(page).not_to have_content "#{product.name}"
    end
  end
end
