require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let(:product) { create(:product, :with_product_properties) }

  scenario "show page" do
    visit potepan_product_path(product.id)
    expect(page).to have_title   "#{product.name} - BIGBAG Store"
    expect(page).to have_content product.name, count: 3
    expect(page).to have_content product.display_price
    expect(page).to have_content product.description
    product.product_properties.each do |product_prop|
      expect(page).to have_content product_prop.value
    end
  end
end
